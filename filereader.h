#pragma once
#include <fstream>
#include <iterator>
#include <iostream>
#include <string>
#include "list.h"
#include "utils.h"
#include "hashmap.h"
#include "vector"

namespace my{

class FileReader
{
public:
    FileReader();
    void load(const std::string& name);
    void read(uint start_line, uint num_of_lines);
    void search(std::string keyword, my::vector<uint>& result, bool no_case_sense);
    my::vector<uint> search(std::string keyword, bool no_case_sense);
    const std::string& buffer() const;
    uint numberOfLines();

private:
    std::string buffer_;
    std::ifstream textfile_;
    my::vector<uint> line_size_;
    my::vector<std::string> tokens_;
    my::vector<std::string> lowercase_tokens_;
    my::hashmap<std::string,std::list<uint> > word_index_;
    uint chunkSize(uint line_index);
    uint chunkSize(uint start_index, uint line_index);
};

}

