
#include "utils.h"

my::vector<std::string> &my::utils::split(const std::string &s, char delim, my::vector<std::string> &elems)
{
    std::stringstream ss(s);
    std::string item;
    while (std::getline(ss, item, delim))
        elems.push_back(item);
    return elems;
}
    
my::vector<std::string> my::utils::split(const std::string &s, char delim)
{
    my::vector<std::string> elems;
    split(s, delim, elems);
    return elems;
}   

void my::utils::searchString(const std::string& source, const std::string& keyword, my::vector<int>& result)
{
    my::vector<std::string> words;
    my::utils::split(source,' ',words);
    int pos = 0;
    for (auto &each_word : words){
        uint word_pos = each_word.find(keyword);
        if (word_pos != std::string::npos){
            pos += word_pos;
            result.push_back(pos);
            pos+=each_word.size() + 1 - word_pos;
        }
        else
            pos+=(each_word.size() + 1);
    }
}

my::vector<int> my::utils::searchString(const std::string& source, const std::string& keyword)
{
    my::vector<int> result;
    searchString(source, keyword, result);
    return result;
}

std::string my::utils::toLowerCase(const std::string &src)
{
    std::string result;
    for(auto ch : src)
        if(ch<='Z' && ch>='A')
             result.push_back(ch-('Z'-'z'));
        else result.push_back(ch);
    return result;
}
