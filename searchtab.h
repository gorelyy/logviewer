#pragma once
#include <QDebug>

#include <QWidget>
#include <QGridLayout>
#include <QListWidget>
#include <QLineEdit>
#include <QPushButton>
#include <QLabel>
#include <QColorDialog>
#include <QScrollBar>
#include <QCheckBox>
#include "searchresultlist.h"
#include "vector.h"

class SearchTab : public QWidget
{
    Q_OBJECT
public:
    explicit SearchTab(QWidget *parent = 0);
    QLineEdit* searchField();
    QScrollBar* scrollBar();
    uint index();
    void setIndex(uint index);
    const std::string searchKey();
    void addSearchResult(int line_index);
    void addSearchResult(int line_index, const QString& line);
    void clear();
    const QColor& searchHighLightColor();
    void updateListSize();
    void updateListScrollBar();
    int maxListItems();
    my::vector<int> scrollLines(int index);
    void loadScrollLine(const QString& text);
    void updateListSelection();
    void outputSearchResultSize(int size);
    void hideSearchResultSize();
    bool noCaseSense();
    QCheckBox *caseCheckBox();
signals:
    void searchFieldUpdated(int, QString);
    void listLineSelected(int);
    void colorSelected();

public slots:
    void emitSearchFieldSignal(QString);
    void emitLineSelected(QListWidgetItem*);
    void setColorFromDialog();
    void setSelection();

private:
    QLineEdit*   search_field_;
    SearchResultList* list_;
    QPushButton* color_button_;
    QColor color_;
    QGridLayout *content_;
    QScrollBar* scrollbar_;
    QLabel * search_result_size_label_;
    QCheckBox *case_checkbox_;
    uint index_;
    my::vector<int> search_result_;
    int list_max_items_;
    int list_first_item_index_;
    int list_selected_item_index_;
    int list_scroll_item_;
    int prev_scroll_value_;
    bool no_case_sense_;
    void updateColorButton();


};


