#pragma once
#include <cstring>

namespace my{
template <typename T>
class vector{
public:
    class iterator{
        friend class vector;
    public:
        iterator() : ptr(nullptr)
        {
        }

        iterator(T* src_ptr):ptr(src_ptr)
        {
        }

        iterator operator++()
        {
            ptr++;
            return *this;
        }

        iterator operator++(int)
        {
            iterator i = *this;
            ptr++;
            return i;
        }

        iterator operator--()
        {
            ptr--;
            return *this;
        }

        iterator operator--(int)
        {
            iterator i = *this;
            ptr--;
            return i;
        }

        iterator operator+=(size_t i)
        {
            ptr+=i;
            return *this;
        }

        iterator operator-=(size_t i)
        {
            ptr-=i;
            return *this;
        }

        iterator operator+(size_t i)
        {
            iterator tmp(ptr);
            tmp+=i;
            return tmp;
        }

        iterator operator-(size_t i)
        {
            iterator tmp(ptr);
            tmp-=i;
            return tmp;
        }

        T& operator*()
        {
            return *ptr;
        }

        T* operator->()
        {
            return ptr;
        }

        bool operator ==(const iterator& src)
        {
            return ptr == src.ptr;
        }

        bool operator !=(const iterator& src)
        {
            return ptr != src.ptr;
        }

    private:
        T* ptr;
    };

    vector():size_(0), capacity_(0),array_(nullptr)
    {
    }

    vector(const vector& src) :size_(0), capacity_(0),array_(nullptr)
    {
        capacity_ = src.capacity_;
        reserve(capacity_);
        size_ = src.size_;
        for (size_t i = 0; i != size_; ++i){
            void* src_element = static_cast<char*>(src.array_) + i*sizeof(T);
            new(static_cast<char*>(array_) + i*sizeof(T)) T(*static_cast<T*>(src_element));
        }

    }

    ~vector()
    {
        deleteArray();
    }

    void clear()
    {
        deleteArray();
        array_ = nullptr;
        size_ = 0;
        capacity_ = 0;
    }

    void push_back(const T& data)
    {
        if (size_ == capacity_)
            reserve(size_ * 2);
        new(static_cast<char*>(array_) + size_*sizeof(T)) T(data);
        size_++;
    }

    void pop_back()
    {
        void* address = static_cast<char*>(array_) + (size_-1)*sizeof(T);
        static_cast<T*>(address)->~T();
        size_--;
    }

    void reserve(size_t newcapacity)
    {
        capacity_ = (newcapacity)?newcapacity : 1;
        void* temp = ::operator new((capacity_) * sizeof(T));
        for (size_t i = 0; i != size_; ++i){
            void* src = static_cast<char*>(array_) + i*sizeof(T);
            new(static_cast<char*>(temp) + i*sizeof(T)) T(*static_cast<T*>(src));
        }
        if(array_)
            deleteArray();
        array_ = temp;
    }

    vector& operator=(vector src)
    {
        swap(src);
        return *this;
    }

    T& operator[](size_t index)
    {
        void* address = static_cast<char*>(array_) + index*sizeof(T);
        return *static_cast<T*>(address);
    }

    void erase(size_t index)
    {
        if (index>=size_)
            return;
        for(;index<size_;++index){
            void* address = static_cast<char*>(array_) + index*sizeof(T);
            static_cast<T*>(address)->~T();
            if (index == size_ - 1) break;
            address = static_cast<char*>(array_) + (index+1)*sizeof(T);
            new(static_cast<char*>(array_) + index*sizeof(T)) T(*static_cast<T*>(address));
        }
        size_--;
    }

    void erase(iterator it)
    {
       size_t index = it.ptr - static_cast<T*>(array_);
       erase(index);
    }

    void swap(vector& src)
    {
        std::swap(size_, src.size_);
        std::swap(capacity_, src.capacity_);
        std::swap(array_, src.array_);
    }

    T& back() const
    {
        void* address = static_cast<char*>(array_) + (size_ -1)*sizeof(T);
        return *static_cast<T*>(address);
    }

    T& front()
    {
        return *static_cast<T*>(array_);
    }

    iterator begin() const
    {
        return iterator(static_cast<T*>(array_));
    }

    iterator end() const
    {
        return iterator(&back() + 1);
    }

    bool empty() const
    {
        return !size_;
    }

    size_t size()
    {
        return size_;
    }
private:

    void deleteArray()
    {
        for (size_t i = 0; i < size_; ++i){
            void* address = static_cast<char*>(array_) + i*sizeof(T);
            static_cast<T*>(address)->~T();
        }
        ::operator delete(array_);

    }

    size_t size_;
    size_t capacity_;
    void* array_;
};
}
