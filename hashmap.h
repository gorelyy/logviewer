#pragma once
#include "list.h"
#include "utils.h"
namespace my{

template <typename Key, typename T>
class hashmap{
const int DEFAULT_SIZE  = 2;
public:
    struct pair{
        Key key;
        T value;

        pair()
        {
        }

        pair(const pair& src_pair)
        {
            key = src_pair.key;
            value = src_pair.value;
        }

        pair& operator=(pair src_pair)
        {
            my::utils::swap(key, src_pair.key);
            my::utils::swap(value, src_pair.value);
            return *this;
        }
        bool operator==(const pair& rval)
        {
            return key == rval.key && value == rval.value;
        }
        bool operator!=(const pair& rval)
        {
            return (key != rval.key || value != rval.value);
        }
    };

    class iterator{

    public:
       iterator() : owner(nullptr), pair_ptr(nullptr)
       {
       }

       iterator(hashmap* owner, pair *p) : owner(owner), pair_ptr(p)
       {
       }

       iterator(const iterator& src)
       {
           owner = src.owner;
           pair_ptr = src.pair_ptr;
       }

       iterator& operator=(iterator src)
       {
           my::utils::swap(owner, src.owner);
           my::utils::swap(pair_ptr, src.pair_ptr);
           return *this;
       }

       iterator operator++()
       {
           uint table_index = owner->hash(pair_ptr->key);
           list<pair> &chain = owner->table_[table_index];
           typename list<pair>::iterator it = chain.find(*pair_ptr);
           if(++it != chain.end()){
               pair_ptr = &(*it);
               return *this;
           }
           do ++table_index;
           while(table_index < owner->capacity_ && !owner->table_[table_index].size());
           if (table_index >= owner->capacity_){
               pair_ptr = nullptr;
               return *this;
           }
           pair_ptr = &owner->table_[table_index].front();
           return *this;
       }

       iterator operator++(int)
       {
           iterator i = *this;
           uint table_index = owner->hash(pair_ptr->key);
           list<pair> &chain = owner->table_[table_index];
           typename list<pair>::iterator it = chain.find(*pair_ptr);
           if(++it != chain.end()){
               pair_ptr = &(*it);
               return i;
           }
           do ++table_index;
           while(table_index < owner->capacity_ && !owner->table_[table_index].size());
           if (table_index >= owner->capacity_){
               pair_ptr = nullptr;
               return i;
           }
           pair_ptr = &owner->table_[table_index].front();
           return i;
       }

       pair& operator*()
       {
           return *pair_ptr;
       }

       pair* operator->()
       {
           return pair_ptr;
       }

       bool operator==(const iterator& rval)
       {
           return pair_ptr == rval.pair_ptr && owner == rval.owner;
       }

       bool operator!=(const iterator& rval)
       {
           return pair_ptr != rval.pair_ptr || owner != rval.owner;
       }

    private:

       hashmap* owner;
       pair* pair_ptr;

    };

    hashmap() : size_(0),
                min_index_(0),
                capacity_(DEFAULT_SIZE),
                max_capacity_(DEFAULT_SIZE * 1.5),
                preserve_table_(false)
    {
        table_ = new my::list<pair>[capacity_];
    }

    hashmap(uint capacity) : size_(0),
                             min_index_(0),
                             capacity_(capacity),
                             max_capacity_(capacity * 1.5),
                             preserve_table_(false)
    {
        table_ = new my::list<pair>[capacity_];
    }

    hashmap(const hashmap& src)
    {
        preserve_table_ = false;
        size_ = src.size_;
        min_index_ = src.min_index_;
        capacity_ = src.capacity_;
        max_capacity_ = src.max_capacity_;
        table_ = new my::list<pair>[capacity_];
        for(int i = 0; i != capacity_; ++i)
            table_[i] = src.table_[i];
    }

    ~hashmap()
    {
        if (!preserve_table_)
            delete[] table_;
    }

    hashmap& operator=(hashmap src_dict)
    {
        swap(src_dict);
        return *this;
    }

    void swap(hashmap& src)
    {
        my::utils::swap(size_, src.size_);
        my::utils::swap(min_index_, src.min_index_);
        my::utils::swap(capacity_, src.capacity_);
        my::utils::swap(max_capacity_, src.max_capacity_);
        my::utils::swap(table_, src.table_);
    }

    iterator begin()
    {
        return iterator(this, &table_[min_index_].front());
    }

    iterator end()
    {
        return iterator(this,nullptr);
    }

    void insert(const Key& key, const T& value)
    {
        if (size_ == max_capacity_)
            rehash();

        uint table_index = hash(key);
        list<pair>& chain = table_[table_index];
        typename list<pair>::iterator it, end;
        for(it = chain.begin(), end = chain.end(); it != end; ++it)
            if (it->key == key){
                it->value = value;
                return;
            }
        pair data;
        data.key = key;
        data.value = value;
        chain.push_front(data);
        if (!size_ || table_index <= min_index_)
           min_index_ = table_index;
        size_++;
    }

    iterator find(const Key& key)
    {
        uint table_index = hash(key);
        list<pair>& chain = table_[table_index];
        if (chain.empty())
            return end();
        for(auto it = chain.begin(), chain_end = chain.end(); it != chain_end; ++it)
            if (it->key == key)
                return iterator(this,&(*it));
        return end();
    }

    void remove(const iterator& it)
    {
        remove(it->key);
    }

    void remove(const Key& key)
    {
        uint table_index = hash(key);
        list<pair>& chain = table_[table_index];
        if (chain.empty())
            return;
        for(auto it = chain.begin(), chain_end = chain.end(); it != chain_end; ++it)
            if (it->key == key){
                chain.remove(it);
                return;
            }
    }

    T& operator[](const Key& key)
    {
        iterator search = find(key);
        if (search != end())
            return (*search).value;

        T newdata;
        insert(key,newdata);
        search = find(key);
        return (*search).value;

    }

    void clear()
    {
        delete[] table_;
        table_ = new my::list<pair>[DEFAULT_SIZE];
        capacity_ = DEFAULT_SIZE;
        max_capacity_ = capacity_ * 1.5;
        size_ = 0;
        min_index_ = 0;
    }

private:

    my::list<pair>*  table_;
    uint        size_;
    uint        min_index_;
    uint        capacity_;
    uint        max_capacity_;
    bool        preserve_table_; //отключает удаление
                                 //массива при вызове деструктора

    unsigned int hash(const Key& key)
    {
        return std::hash<Key>()(key) % capacity_;
    }

    void rehash()
    {
        hashmap<Key,T> temp(max_capacity_ * 2  + 1);
        for (auto it = begin(); it != end(); ++it)
            temp[it->key] = it->value;
        capacity_ = max_capacity_* 2 + 1;
        max_capacity_ = capacity_ * 1.5;
        temp.preserve_table_ = true;
        delete[] table_;
        table_ = temp.table_;
        min_index_ = temp.min_index_;
    }
};

}

