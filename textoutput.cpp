#include "textoutput.h"

TextOutput::TextOutput(QWidget *parent) :
    QPlainTextEdit(parent),
    first_line_index_(0)
{
    setLineWrapMode(QPlainTextEdit::NoWrap);
    setReadOnly(true);

    line_numbers_ = new LineNumbers(this);
    scrollbar_ = new QScrollBar;
    scrollbar_->setMinimum(0);
    scrollbar_->setMaximum(0);

    connect(this, SIGNAL(blockCountChanged(int)), this, SLOT(updateLineNumbersWidth(int)));
    connect(this, SIGNAL(updateRequest(QRect,int)), this, SLOT(updateLineNumbers(QRect,int)));

    updateLineNumbersWidth(0);

    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}

int TextOutput::lineNumbersWidth()
{
    int digits = 1 + log10(first_line_index_ + heightInLines());
    return 3 + fontMetrics().width(QLatin1Char('9')) * digits;
}



void TextOutput::updateLineNumbersWidth(int)
{
    setViewportMargins(lineNumbersWidth(), 0, 0, 0);
}



void TextOutput::updateLineNumbers(const QRect &rect, int dy)
{
    if (dy)
        line_numbers_->scroll(0, dy);
    else
        line_numbers_->update(0, rect.y(), line_numbers_->width(), rect.height());

    if (rect.contains(viewport()->rect()))
        updateLineNumbersWidth(0);
}



void TextOutput::resizeEvent(QResizeEvent *e)
{
    QPlainTextEdit::resizeEvent(e);
    QRect cr = contentsRect();
    line_numbers_->setGeometry(QRect(cr.left(), cr.top(), lineNumbersWidth(), cr.height()));
}

void TextOutput::wheelEvent(QWheelEvent *e)
{
    int  dy =((-e->delta()/8)/15)*scrollbar_->singleStep();
    scrollbar_->setSliderPosition(scrollbar_->sliderPosition() + dy);
    e->ignore();
}

void TextOutput::lineNumbersPaintEvent(QPaintEvent *event)
{
    QPainter painter(line_numbers_);
    painter.fillRect(event->rect(), Qt::lightGray);


    QTextBlock block = firstVisibleBlock();
    int blockNumber = first_line_index_;
    int top = (int) blockBoundingGeometry(block).translated(contentOffset()).top();
    int bottom = top + (int) blockBoundingRect(block).height();

    while (block.isValid() && top <= event->rect().bottom()) {
        if (block.isVisible() && bottom >= event->rect().top()) {
            QString number = QString::number(blockNumber + 1);
            painter.setPen(Qt::black);
            if (!toPlainText().isEmpty())
                painter.drawText(0, top, line_numbers_->width(), fontMetrics().height(),
                                 Qt::AlignRight, number);
        }
        block = block.next();
        top = bottom;
        bottom = top + (int) blockBoundingRect(block).height();
        ++blockNumber;
    }
}

void TextOutput::updateFirstLineIndex(uint index)
{
    first_line_index_ = index;
}

int TextOutput::heightInLines()
{
    return contentsRect().height() / fontMetrics().height() + 1;
}

int TextOutput::firstLineIndex()
{
    return first_line_index_;
}

QScrollBar* TextOutput::scrollBar()
{
    return scrollbar_;
}

void TextOutput::setTextSizeInLines(uint number_of_lines)
{
    scrollbar_->setMaximum(number_of_lines - heightInLines() + 1);
    scrollbar_->setPageStep(heightInLines());
}

void TextOutput::setScrollBarPosition(uint index)
{
    scrollbar_->setSliderPosition(index);
}
