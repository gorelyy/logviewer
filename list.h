#pragma once
#include <QDebug>
#include "utils.h"

namespace my{
template<typename T>
class list
{
    struct list_node
    {
        list_node():next(nullptr){}
        list_node* next;
        T data;
    };

public:
    class iterator{
        friend class list;

    public:
       iterator() : node(nullptr)
       {
       }

       iterator(list_node *n) : node(n)
       {
       }

       iterator operator++()
       {
           node = node ? node->next : nullptr;
           return *this;
       }

       iterator operator++(int)
       {
           iterator i = *this;
           node = node ? node->next : nullptr;
           return i;
       }

       T& operator*()
       {
           return node->data;
       }

       T* operator->()
       {
           return &node->data;
       }

       bool operator==(const iterator& rval)
       {
           return node == rval.node;
       }

       bool operator!=(const iterator& rval)
       {
           return node != rval.node;
       }

    private:
       list_node* node;

    };

    list():first_(nullptr), size_(0)
    {
    }

    ~list()
    {
        clear();
    }

    list(const list& copy_list) : first_(nullptr)
    {
        list_node *node = nullptr,
                  *prev_node = nullptr,
                  *copy_node = copy_list.first_;

        while(copy_node){
            if (!prev_node){
                prev_node = new list_node;
                prev_node->data = copy_node->data;
                first_ = prev_node;
            }
            else{
                node = new list_node;
                node->data = copy_node->data;
                prev_node->next = node;
                prev_node = node;
            }
            copy_node = copy_node->next;
        }
        size_ = copy_list.size_;
    }

    list& operator=(list other_list)
    {
        swap(other_list);
        return *this;
    }

    iterator begin()
    {
        return iterator(first_);
    }

    iterator end()
    {
        return iterator(nullptr);
    }


    iterator find(const T &data)
    {
        for (list_node *each_node = first_; each_node; each_node = each_node->next){
            if (each_node->data == data)
                return iterator(each_node);
        }
        return end();
    }

    void push_front(const T &data)
    {
        list_node* node = new list_node;
        node->data = data;
        node->next = first_;
        first_ = node;
        size_++;
    }

    void remove(const iterator& it)
    {
        if (it.node == nullptr)
            return;

        if(it.node == first_){
            list_node* temp = first_->next;
            delete first_;
            first_ = temp;
            size_--;
        }        
        else{
            for (list_node* each_node = first_; each_node; each_node = each_node->next){
                if (each_node->next == it.node){
                    each_node->next = it.node->next;
                    delete it.node;
                    size_--;
                }
            }
        }
    }

    void clear()
    {
        list_node* each = first_;
        while(each){
            list_node* temp = each->next;
            delete each;
            each = temp;
        }
        size_ = 0;
        first_ = nullptr;
    }

    void swap(list& other)
    {
        my::utils::swap(first_,other.first_);
        my::utils::swap(size_, other.size_);
    }

    uint size() const
    {
        return size_;
    }

    bool empty() const
    {
        return !size_;
    }

    T& front()
    {
        return first_->data;
    }

    const T& front() const
    {
        return first_->data;
    }


private:

    list_node* first_;
    uint size_;
};
}

