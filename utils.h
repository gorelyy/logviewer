
#pragma once
#include <string>
#include <sstream>
#include <math.h>
#include "vector.h"
typedef unsigned int uint;

namespace my{
namespace utils
{
    template <class T>
    std::string toString (const T& t)
    {
        std::stringstream ss;
        ss << t;
        return ss.str();
    }

    template <class T>
    void quickSort(my::vector<T> &src, int left, int right)
    {
       int i=left, j=right;
       int pivot = src[(i+j)/2];

       while (i <= j) {
           while (src[i] < pivot)
               i++;

           while (src[j] > pivot)
               j--;

           if (i <= j) {
               swap(src[i],src[j]);
               i++;
               j--;
           }
       }

       if (left < j)
           quickSort(src, left, j);

       if (i < right)
           quickSort(src, i, right);
    }

    template <class T>
    void quickSort(my::vector<T> &src)
    {
        quickSort(src,0,src.size()-1);
    }

    template <class T>
    void swap(T&a, T&b)
    {
        T temp(a);
        a = b;
        b = temp;
    }

    my::vector<std::string>&   split(const std::string &s, char delim, my::vector<std::string> &elems);
    my::vector<std::string>    split(const std::string &s, char delim);
    double                     round(double number);
    my::vector<int> searchString(const std::string& source, const std::string& keyword);
    std::string toLowerCase(const std::string& src);
    void searchString(const std::string& source, const std::string& keyword, my::vector<int>& result);
}

}
