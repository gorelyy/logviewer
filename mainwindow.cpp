#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent), file_(nullptr)
{
    setMinimumSize(640,600);
    window_widget_ = new QWidget;
    setCentralWidget(window_widget_);

    window_ = new QVBoxLayout;
    window_widget_->setLayout(window_);
    splitter_ = new QSplitter;
    splitter_->setOrientation(Qt::Vertical);
    window_->addWidget(splitter_);
    text_output_ = new TextOutput;
    QHBoxLayout *text_output_layout = new QHBoxLayout;
    text_output_layout->addWidget(text_output_);
    text_output_layout->addWidget(text_output_->scrollBar());
    QWidget* text_area = new QWidget;
    text_area->setLayout(text_output_layout);
    splitter_->addWidget(text_area);
    search_browser_layout_ = new QHBoxLayout;
    QWidget *search_area = new QWidget;
    search_area->setLayout(search_browser_layout_);
    splitter_->addWidget(search_area);
    splitter_->setCollapsible(0,false);
    splitter_->setCollapsible(1,true);
    splitter_->setSizes(QList<int>()<<300<<300);
    addActions();
    addMenu();
    addSearchBrowser();
    setWindowTitle("logviewer");
    loading_progress_bar_ = new QProgressBar;
    window_->addWidget(loading_progress_bar_);
    loading_progress_bar_->setMinimum(0);
    loading_progress_bar_->setMaximum(0);
    loading_progress_bar_->hide();
    file_information_ = new QLabel;
    window_->addWidget(file_information_);
    file_information_->hide();
    connect(text_output_->scrollBar(), SIGNAL(valueChanged(int)), this, SLOT(scroll(int)));
    connect(&future_watcher_, SIGNAL(finished()), this, SLOT(loadingFinished()));
    connect(&future_watcher_, SIGNAL(progressValueChanged(int)),  loading_progress_bar_,SLOT(setValue(int)));
    connect(splitter_,SIGNAL(splitterMoved(int,int)),this,SLOT(updateSize(int,int)));
}

MainWindow::~MainWindow()
{
}

void MainWindow::addActions()
{
    open_file_action_ = new QAction("Открыть",this);
    connect(open_file_action_, SIGNAL(triggered()), this, SLOT(openFile()));
    exit_app_action_ = new QAction("Выход",this);
    connect(exit_app_action_, SIGNAL(triggered()), this, SLOT(close()));
}

void MainWindow::addMenu()
{
    file_menu_ = menuBar()->addMenu("Файл");
    file_menu_->addAction(open_file_action_);
    file_menu_->addAction(exit_app_action_);
}

//SLOT:
void MainWindow::openFile()
{
    if (file_)
        closeFile();
    file_ = new my::FileReader;
    QString filename =  QFileDialog::getOpenFileName(this, tr("Open File"),"",tr("Files (*.*)"));

    if (filename.isNull()) return;
    loading_progress_bar_->show();
    file_information_->setText(filename);
    QFuture<void> future = QtConcurrent::run(file_, &my::FileReader::load, filename.toStdString());
    future_watcher_.setFuture(future);

}

//SLOT
void MainWindow::loadingFinished()
{

    loading_progress_bar_->hide();
    file_information_->show();
    file_->read(0, text_output_->heightInLines());
    text_output_->setTextSizeInLines(file_->numberOfLines());
    text_output_->setPlainText(QString::fromStdString(file_->buffer()));
    search_browser_->setEnabled(true);

}

//SLOT:
void MainWindow::controlTabs(int index)
{
    uint uindex = index;
    if (uindex != search_browser_->plusTabIndex()){
        for(uint i = 0; i != search_browser_->numberOfTabs(); ++i)
            if (i != search_browser_->previousTab()->index())
                updateSearchTabSize(search_browser_->tab(i));
        search_browser_->setPreviousTab();
        return;
    }

    SearchTab* newtab = search_browser_->newTab("...");
    connect(newtab->searchField(), SIGNAL(textChanged(QString)), this, SLOT(startSearch(QString)));
    connect(newtab,SIGNAL(listLineSelected(int)),this,SLOT(jumpToLine(int)));
    connect(newtab->scrollBar(), SIGNAL(valueChanged(int)),this, SLOT(loadSearchLinesOnScroll(int)));
    connect(newtab->caseCheckBox(),SIGNAL(clicked()),this,SLOT(startSearch()));
    connect(newtab,SIGNAL(colorSelected()),this,SLOT(highlightSearchResult()));
}

//SLOT:
void MainWindow::scroll(int index)
{
    if (!file_)
        return;
    index = std::max(index, 0);
    text_output_->updateFirstLineIndex(index);
    file_->read(index, text_output_->heightInLines());
    text_output_->setPlainText(QString::fromStdString(file_->buffer()));
    highlightSearchResult();
}

void MainWindow::resizeEvent(QResizeEvent*)
{
    updateSize(0,0);//dummy values
}

//SLOT
void MainWindow::startSearch(QString search_key)
{
    if (!file_)
        return;

    if(search_key.isEmpty()){
        search_browser_->currentTab()->clear();
        highlightSearchResult();
        return;
    }

    SearchTab* tab = search_browser_->currentTab();
    tab->clear();
    int prev_index = -1;
    my::vector<uint> search_result;
    file_->search(search_key.toStdString(), search_result, tab->noCaseSense());
    my::utils::quickSort(search_result);

    int i = 0;
    for(int index : search_result){
        if (index == prev_index) continue;
        if (i < tab->maxListItems()){
            file_->read(index,1);
            tab->addSearchResult(index, searchLineOutputFormat(index,file_->buffer()));
        }
        else
            tab->addSearchResult(index);
        i++;
        prev_index = index;
    }
    tab->updateListScrollBar();
    tab->outputSearchResultSize(search_result.size());
    highlightSearchResult();
}

void MainWindow::startSearch()
{
    startSearch(QString::fromStdString(search_browser_->currentTab()->searchKey()));
}
//SLOT
void MainWindow::jumpToLine(int index)
{
    //scroll(index);
    text_output_->setScrollBarPosition(index);
    highlightSearchResult();
}


void MainWindow::highlightSearchResult()
{
    QTextCursor cursor = text_output_->textCursor();
    cursor.select(QTextCursor::Document);
    cursor.setCharFormat(cursor.charFormat());

    uint pos = 0, word_pos = 0;
    for(auto &word : my::utils::split(text_output_->toPlainText().toStdString(),' ')){
        for(uint i = 0; i < search_browser_->numberOfTabs(); ++i){
            SearchTab *tab = search_browser_->tab(i);
            word_pos = (tab->noCaseSense())?my::utils::toLowerCase(word).find(my::utils::toLowerCase(tab->searchKey())) : word.find(tab->searchKey());
            if (word_pos != std::string::npos){
                cursor.setPosition(pos + word_pos);
                cursor.setPosition(pos + word_pos + tab->searchKey().size(), QTextCursor::KeepAnchor);
                QTextCharFormat format = cursor.charFormat();
                format.setBackground(QBrush(tab->searchHighLightColor()));
                cursor.setCharFormat(format);
            }
        }
        pos += word.size() + 1;
    }
}

void MainWindow::loadSearchLinesOnScroll(int index)
{
    if (!file_)
        return;

     SearchTab *tab = search_browser_->currentTab();
     if (tab->searchKey().empty())
         return;

     const auto &lines = tab->scrollLines(index);
     if( lines.empty()){
         return;
     }
     for (int line_index : lines){
         file_->read(line_index,1);
         tab->loadScrollLine(searchLineOutputFormat(line_index,file_->buffer()));
     }
     tab->updateListSelection();
}

void MainWindow::closeFile()
{
    delete file_;
    file_ = nullptr;
    search_browser_layout_->removeWidget(search_browser_);
    delete search_browser_;
    addSearchBrowser();
    text_output_->clear();
    text_output_->updateFirstLineIndex(0);
    file_information_->hide();
}

void MainWindow::updateSearchTabSize(SearchTab *tab)
{
    tab->updateListSize();
    if (!tab->searchKey().empty() && file_){
        for (int line_index : tab->scrollLines(tab->scrollBar()->sliderPosition())){
            file_->read(line_index,1);
            tab->loadScrollLine(searchLineOutputFormat(line_index,file_->buffer()));
        }
        tab->updateListSelection();
    }
}

void MainWindow::addSearchBrowser()
{
    search_browser_ = new SearchBrowser;
    SearchTab* newtab = search_browser_->newTab("...");
    search_browser_layout_->addWidget(search_browser_);
    search_browser_->setEnabled(false);

    connect(newtab->scrollBar(), SIGNAL(valueChanged(int)),this, SLOT(loadSearchLinesOnScroll(int)));
    connect(newtab->searchField(), SIGNAL(textChanged(QString)), this, SLOT(startSearch(QString)));
    connect(newtab,SIGNAL(listLineSelected(int)),this,SLOT(jumpToLine(int)));
    connect(newtab,SIGNAL(colorSelected()),this,SLOT(highlightSearchResult()));
    connect(newtab->caseCheckBox(),SIGNAL(clicked()),this,SLOT(startSearch()));
    connect(search_browser_,SIGNAL(currentChanged(int)), this, SLOT(controlTabs(int)));
    connect(search_browser_,SIGNAL(tabCloseRequested(int)),this,SLOT(highlightSearchResult(int)));
}

QString MainWindow::searchLineOutputFormat(int line_index, const std::string &text)
{
    return "[строка " + QString::number(line_index + 1) + "] "+ QString::fromStdString(text);
}

void MainWindow::highlightSearchResult(int)
{
    highlightSearchResult();
}

//SLOT
void MainWindow::updateSize(int,int)
{
    updateSearchTabSize(search_browser_->currentTab());
    if (!file_)
        return;
    text_output_->setTextSizeInLines(file_->numberOfLines());
    file_->read(text_output_->firstLineIndex(), text_output_->heightInLines());
    text_output_->setPlainText(QString::fromStdString(file_->buffer()));
    highlightSearchResult();
}
