#include "searchresultlist.h"

SearchResultList::SearchResultList(QWidget *parent) :
    QListWidget(parent)
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
}

void SearchResultList::setScrollBar(QScrollBar *scrollbar)
{
    scrollbar_ = scrollbar;
}

void SearchResultList::wheelEvent(QWheelEvent *e)
{
    int  dy =((-e->delta()/8)/15)*scrollbar_->singleStep();
    scrollbar_->setSliderPosition(scrollbar_->sliderPosition() + dy);
    e->ignore();
}
