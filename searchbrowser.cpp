#include "searchbrowser.h"

SearchBrowser::SearchBrowser(QWidget *parent) :
    QTabWidget(parent)
{
    plus_tab_index_ = 0;
    previous_tab_ = 0;
    plustab_ = new QWidget;
    addTab(plustab_,"+");
    setTabsClosable(true);
    tabBar()->tabButton(0, QTabBar::RightSide)->resize(0, 0);

    connect(this,SIGNAL(tabCloseRequested(int)),this, SLOT(closeTab(int)));
}

SearchTab* SearchBrowser::newTab(const QString &name)
{
    SearchTab* tab = new SearchTab;
    tabs_.push_back(tab);
    tab->setIndex(plus_tab_index_);
    insertTab(plus_tab_index_,tab, name);
    setCurrentIndex(plus_tab_index_++);
    connect(tab,SIGNAL(searchFieldUpdated(int,QString)),this,SLOT(changeTabName(int,QString)));
    return tab;

}


SearchTab* SearchBrowser::tab(uint index)
{
    if (index >= tabs_.size())
        return nullptr;
    return tabs_[index];
}

SearchTab* SearchBrowser::currentTab()
{
    return tab(currentIndex());
}

uint SearchBrowser::plusTabIndex()
{
    return plus_tab_index_;
}

void SearchBrowser::changeTabName(int index, QString name)
{
    setTabText(index, name);
}

void SearchBrowser::closeTab(int index)
{
    uint uindex = index;
    if (!uindex && plus_tab_index_ == 1)
        return;

    for(uint i = uindex + 1; i != tabs_.size(); ++i)
        tabs_[i]->setIndex(tabs_[i]->index() - 1);
    if (uindex == plus_tab_index_ - 1)
        setCurrentIndex(uindex - 1);
    removeTab(uindex);
    tabs_.erase(tabs_.begin() + uindex);
    plus_tab_index_--;
}

uint SearchBrowser::numberOfTabs()
{
    return tabs_.size();
}

SearchTab* SearchBrowser::previousTab()
{
    return tabs_[previous_tab_];
}

void SearchBrowser::setPreviousTab()
{
    previous_tab_ = currentIndex();
}
