QT       += core gui concurrent

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets


TARGET = log-viewer
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    textoutput.cpp \
    filereader.cpp \
    utils.cpp \
    searchtab.cpp \
    searchbrowser.cpp \
    searchresultlist.cpp

HEADERS  += mainwindow.h \
    list.h \
    textoutput.h \
    filereader.h \
    utils.h \
    searchtab.h \
    searchbrowser.h \
    searchresultlist.h \
    vector.h \
    hashmap.h

FORMS    += mainwindow.ui

OTHER_FILES +=

