#include "searchtab.h"

SearchTab::SearchTab(QWidget *parent) :
    QWidget(parent),
    list_first_item_index_(0),
    list_scroll_item_(0),
    prev_scroll_value_(0), no_case_sense_(true)
{
    content_ = new QGridLayout;
    search_field_ = new QLineEdit;
    list_ = new SearchResultList;
    color_button_ = new QPushButton;
    color_.setRgb(255,170,0);
    updateColorButton();
    scrollbar_ = new QScrollBar;
    scrollbar_->hide();
    scrollbar_->setMinimum(0);
    list_->setScrollBar(scrollbar_);
    search_result_size_label_= new QLabel;
    case_checkbox_ = new QCheckBox("Чувствительность к регистру");
    case_checkbox_->setChecked(true);
    case_checkbox_->setEnabled(true);
    content_->addWidget(color_button_,0,0);
    content_->addWidget(search_field_,0,1);
    content_->addWidget(list_,1,1);
    content_->addWidget(scrollbar_,1,1,1,1,Qt::AlignRight);

    content_->addWidget(search_result_size_label_,2,1);
    content_->addWidget(case_checkbox_,2,1,1,1,Qt::AlignRight);
    list_->setSelectionMode(QAbstractItemView::SingleSelection);
    list_->setAutoScroll(false);
    list_->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    list_->adjustSize();
    updateListSize();
    setLayout(content_);
    connect(search_field_,SIGNAL(textChanged(QString)), this, SLOT(emitSearchFieldSignal(QString)));
    connect(list_,SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(emitLineSelected(QListWidgetItem*)));
    connect(color_button_,SIGNAL(clicked()),this, SLOT(setColorFromDialog()));
    connect(list_,SIGNAL(itemClicked(QListWidgetItem*)),this, SLOT(setSelection()));
}

void SearchTab::setIndex(uint index)
{
    index_ = index;
}

uint SearchTab::index()
{
    return index_;
}

//SLOT
void SearchTab::emitSearchFieldSignal(QString search_key)
{
    emit searchFieldUpdated(index_, search_key);
}

//SLOT
void SearchTab::emitLineSelected(QListWidgetItem*)
{
    emit listLineSelected(search_result_[list_->currentRow() + list_first_item_index_]);
}

QLineEdit* SearchTab::searchField()
{
    return search_field_;
}

const std::string SearchTab::searchKey()
{
    return search_field_->text().toStdString();
}

void SearchTab::addSearchResult(int line_index)
{
    search_result_.push_back(line_index);
}

void SearchTab::addSearchResult(int line_index, const QString& line)
{
    list_->addItem(line);
    search_result_.push_back(line_index);
}

void SearchTab::clear()
{
    list_->clear();
    search_result_.clear();
    search_result_size_label_->clear();
}

//SLOT
void SearchTab::setColorFromDialog()
{
    color_ = QColorDialog::getColor();
    updateColorButton();
    emit colorSelected();
}


void SearchTab::updateColorButton()
{
    QString qss = QString("background-color:rgb(%1,%2,%3); border:none;").arg(QString::number(color_.red()),
                                                          QString::number(color_.green()),
                                                          QString::number(color_.blue()));
    color_button_->setStyleSheet(qss);
}

const QColor& SearchTab::searchHighLightColor()
{
    return color_;
}

void SearchTab::updateListSize()
{
    list_->addItem("TEST\n");
    list_max_items_ = list_->height() / list_->visualItemRect(list_->item(0)).height();
    scrollbar_->setMaximum(search_result_.size() - list_max_items_);
    scrollbar_->setMinimum(0);
    scrollbar_->show();
    delete list_->item(0);
}

void SearchTab::updateListScrollBar()
{
    scrollbar_->show();
    scrollbar_->setMaximum(search_result_.size() - list_max_items_);
    scrollbar_->setMinimum(0);
    scrollbar_->setPageStep(list_max_items_);
}

int SearchTab::maxListItems()
{
    return list_max_items_;
}

QScrollBar* SearchTab::scrollBar()
{
    return scrollbar_;
}

my::vector<int> SearchTab::scrollLines(int index)
{
    uint uindex = index;
    list_first_item_index_ = uindex;
    list_->clear();
    my::vector<int> result;
    for (uint i = uindex; i != uindex + list_max_items_; ++i)
        if (i < search_result_.size())
        result.push_back(search_result_[i]);
    return result;
}

void SearchTab::loadScrollLine(const QString &text)
{
    list_->addItem(text);
}

void SearchTab::setSelection()
{
    list_selected_item_index_ = list_first_item_index_ + list_->currentRow();
}

void SearchTab::updateListSelection()
{
    int display_index = list_selected_item_index_ - list_first_item_index_;
    if (display_index >=0 && display_index < list_max_items_)
        list_->setCurrentRow(display_index);
    else
        list_->setCurrentRow(-1);
}

void SearchTab::outputSearchResultSize(int size)
{
    search_result_size_label_->setText("Найдено "+QString::number(size)+" cовпадений");
    //search_result_size_label_->show();
}

void SearchTab::hideSearchResultSize()
{
    search_result_size_label_->hide();
}

bool SearchTab::noCaseSense()
{
    return !case_checkbox_->isChecked();
}

QCheckBox * SearchTab::caseCheckBox()
{
    return case_checkbox_;
}
