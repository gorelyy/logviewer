#pragma once
#include <QPlainTextEdit>
#include <QWidget>
#include <QPainter>
#include <QTextBlock>
#include <QScrollBar>
#include <QDebug>

class TextOutput : public QPlainTextEdit
{
    Q_OBJECT
public:
    explicit TextOutput(QWidget *parent = 0);

    void lineNumbersPaintEvent(QPaintEvent* event);
    int  lineNumbersWidth();
    int  heightInLines();
    void updateFirstLineIndex(uint index);
    int firstLineIndex();
    void setTextSizeInLines(uint number_of_lines);
    QScrollBar* scrollBar();
    void setScrollBarPosition(uint index);

public slots:

    void updateLineNumbersWidth(int);
    void updateLineNumbers(const QRect &, int);

protected:

    void resizeEvent(QResizeEvent *event);
    void wheelEvent(QWheelEvent *e);

private:
    QWidget* line_numbers_;
    QScrollBar* scrollbar_;
    int first_line_index_;
};

class LineNumbers : public QWidget
{
public:
    explicit LineNumbers(TextOutput* text_output) : QWidget(text_output),
                                                    text_output_(text_output)
    {
    }
    QSize sizeHint() const
    {
        return QSize(text_output_->lineNumbersWidth(), 0);
    }

protected:

    void paintEvent(QPaintEvent *event)
    {
        text_output_->lineNumbersPaintEvent(event);
    }


private:
    TextOutput* text_output_;
};
