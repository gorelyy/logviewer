#pragma once
#include <QTabWidget>
#include <QWidget>
#include "searchtab.h"


class SearchBrowser : public QTabWidget
{
    Q_OBJECT

public:
    explicit SearchBrowser(QWidget *parent = 0);
    SearchTab* newTab(const QString& name);
    SearchTab* tab(uint index);
    SearchTab* currentTab();
    uint plusTabIndex();
    uint numberOfTabs();
    void init();
    SearchTab* previousTab();
    void setPreviousTab();

signals:

public slots:
    void changeTabName(int index, QString name);
    void closeTab(int index);

private:
    my::vector<SearchTab*> tabs_;
    QWidget* plustab_;
    uint plus_tab_index_;
    uint previous_tab_;
};

