#pragma once
#include <QListWidget>
#include <QScrollBar>
#include <QWheelEvent>
#include <QWidget>

class SearchResultList : public QListWidget
{
    Q_OBJECT
public:
    explicit SearchResultList(QWidget *parent = 0);
    void setScrollBar(QScrollBar* scrollbar);
    void wheelEvent(QWheelEvent *);
signals:

public slots:

private:
    QScrollBar* scrollbar_;
};
