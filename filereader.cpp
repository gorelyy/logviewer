#include "filereader.h"

using namespace my;

FileReader::FileReader()
{
}

void FileReader::load(const std::string& filename)
{
    textfile_.open(filename, std::ios::in);
    textfile_.clear();
    textfile_.seekg(0, textfile_.beg);
    std::string line;
    int index = 0;
    while(std::getline(textfile_, line)){
        //Linux
        //line_size_.push_back(line.size() + 1);
        //
        line_size_.push_back(line.size() + 2);
        //
        std::istringstream linestream(line);
        std::istream_iterator<std::string> eachword(linestream), end;
        for(;eachword != end;++eachword)
            word_index_[*eachword].push_back(index);
        index++;
    }
    line_size_.push_back(3);
    line_size_.push_back(3);
    for (auto &pair: word_index_){
        tokens_.push_back(pair.key);
        lowercase_tokens_.push_back(my::utils::toLowerCase(pair.key));
    }
}


const std::string& FileReader::buffer() const
{
    return buffer_;
}

void FileReader::read(uint start_line, uint num_of_lines)
{
    if(!textfile_.is_open())
        return;
    num_of_lines = std::min(num_of_lines,numberOfLines());
    buffer_.clear();
    buffer_.resize(chunkSize(start_line, start_line + num_of_lines));
    textfile_.clear();
    textfile_.seekg(chunkSize(start_line), textfile_.beg);
    textfile_.read(&buffer_[0], buffer_.size());
}

uint FileReader::chunkSize(uint start_index, uint line_index)
{
    if (!line_index)
        return 0;

    uint result = 0;
    uint last_index = std::min(line_index, line_size_.size());
    for (uint i = start_index; i != last_index; ++i)
        result += line_size_[i];
    return result;
}

uint FileReader::chunkSize(uint line_index)
{
    return FileReader::chunkSize(0,line_index);
}

uint FileReader::numberOfLines()
{
    return line_size_.size();
}

void FileReader::search(std::string keyword, my::vector<uint>& result, bool no_case_sense)
{
    my::vector<std::string> *src = &tokens_;
    my::vector<std::string> search_keys;
    if (no_case_sense){
        src = &lowercase_tokens_;
        keyword = my::utils::toLowerCase(keyword);
    }
    auto &tokens = *src;
    for (size_t i = 0; i != tokens.size(); ++i)
        if (tokens[i].find(keyword) != std::string::npos)
            search_keys.push_back(tokens_[i]);
    for(auto &each_key : search_keys)
        for (auto each : word_index_[each_key])
            result.push_back(each);
}

my::vector<uint> FileReader::search(std::string keyword, bool no_case_sense)
{
    my::vector<uint> result;
    search(keyword, result, no_case_sense);
    return result;
}
