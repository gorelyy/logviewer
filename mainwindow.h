#pragma once
#include <QMainWindow>
#include <QMenu>
#include <QMenuBar>
#include <QAction>
#include <QFileDialog>
#include <QDebug>
#include <QTabWidget>
#include <searchbrowser.h>
#include <QGridLayout>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QTextCursor>
#include <QSplitter>
#include <QProgressBar>
#include <QFutureWatcher>
#include <QtConcurrent/QtConcurrent>
#include "filereader.h"
#include "list.h"
#include "textoutput.h"

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();


protected:
    void resizeEvent(QResizeEvent *);

public slots:
    void openFile();
    void closeFile();
    void controlTabs(int index);
    void scroll(int index);
    void startSearch(QString);
    void startSearch();
    void jumpToLine(int index);
    void loadSearchLinesOnScroll(int);
    void test();


private:

    QMenu           *file_menu_;

    QWidget         *window_widget_;
    QVBoxLayout     *window_;
    QHBoxLayout     *search_browser_layout_;
    QSplitter       *splitter_;

    QAction         *open_file_action_;
    QAction         *exit_app_action_;
    SearchBrowser   *search_browser_;
    TextOutput      *text_output_;

    my::FileReader  *file_;

    QProgressBar    *loading_progress_bar_;
    QLabel          *file_information_;
    QFutureWatcher<void> future_watcher_;

    void addActions();
    void addMenu();
    void addSearchBrowser();
    void highlightWord(const std::string& word, const QColor &color);
    void updateSearchTabSize(SearchTab *tab);
    QString searchLineOutputFormat(int line_index, const std::string& text);

private slots:
    void highlightSearchResult(int);
    void highlightSearchResult();
    void loadingFinished();
    void updateSize(int, int);
};
